#!/bin/bash
IMAGE="node"
TAG="latest"

docker rm -f $IMAGE
docker rmi -f anilornd/$IMAGE:$TAG
docker build --rm -t anilornd/$IMAGE:$TAG .
./run.sh 
