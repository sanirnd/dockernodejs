#!/bin/sh

# git clone on deploy
if [ ! -d app ] ; then
  mkdir logs
  git clone ${GIT_REPOSITORY} app
fi

cd app
node ${INDEX_FILE}
